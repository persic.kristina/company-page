# Company page

This is a company page made for practice!

## Technologies

Project is created with:

- HTML
- SCSS
- Glide.js

For compiling SCSS I was using Koala. You can download it at this [link](http://koala-app.com/).

## Installation

Install it locally using npm:

```
$ npm install
```

<p align="center">
<img src="./images/panda3.png" width="300" height="auto" >
</p>
