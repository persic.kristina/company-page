const config = {
  type: "carousel",
  perView: 3,
  focusAt: "center",
  gap: 25,
  peek: {
    before: 100,
    after: 100,
  },

  breakpoints: {
    1500: {
      perView: 2,
    },
    950: {
      perView: 1,
      gap: 20,
      peek: {
        before: 30,
        after: 30,
      },
    },
  },
  focusAt: 0,
};
new Glide(".glide", config).mount();
